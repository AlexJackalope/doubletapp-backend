from django.urls import path
from .transport.rest.handlers import Me


urlpatterns = [
    path('me', Me.as_view())
]
