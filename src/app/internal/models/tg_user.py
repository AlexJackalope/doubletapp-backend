from django.db import models


class TgUser(models.Model):
    name = models.CharField(max_length=100, unique=True)
    phone = models.CharField(max_length=12)

    def __str__(self):
        return f"{self.name}, phone: {self.phone}"

    def get_info(self):
        return f"User: {self.name}\nPhone: {self.phone}"
