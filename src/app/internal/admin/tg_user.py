from django.contrib import admin

from ..models.tg_user import TgUser


@admin.register(TgUser)
class TgUserAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'phone')
