import re
from telegram import Update
from telegram.ext import CallbackContext

from ...models.tg_user import TgUser


def start(update: Update, context: CallbackContext):
    user, created = TgUser.objects.get_or_create(name=update.effective_user.username)
    if created:
        message = "You are successfully registered! Add your phone number with /set_phone command."
    else:
        if user.phone == '':
            message = "You are already registered, add your phone number with " \
                      "/set_phone command to continue working with the bot."
        else:
            message = f"Hello, {user.name}! We are ready to start."
    context.bot.send_message(chat_id=update.effective_chat.id, text=message)


def set_phone(update: Update, context: CallbackContext):
    user, created = TgUser.objects.get_or_create(name=update.effective_chat.username)
    if user.phone == '':
        message = "Your phone number was added."
    else:
        message = "Your phone number was updated."
    try:
        phone = update.effective_message.text.split(' ')[1]
        if re.fullmatch(r'\+[0-9]{11}', phone) is not None:
            TgUser.objects.filter(name=update.effective_chat.username).update(phone=phone)
        else:
            message = "Please, enter your phone number with + and without any separators."
    except IndexError:
        message = "Please, enter your phone after command"
    context.bot.send_message(chat_id=update.effective_chat.id, text=message)


def me(update: Update, context: CallbackContext):
    user, created = TgUser.objects.get_or_create(name=update.effective_chat.username)
    if user.phone == '':
        _no_phone_message(update, context)
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text=f'Your info.\n{user.get_info()}')


def _no_phone_message(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=update.effective_chat.id, text='To add your phone number type it in message '
                                                                    r"after /set_phone command. Otherwise you won't "
                                                                    'be able to work')
