from django.views import View
from django.http import HttpResponse

from ...models.tg_user import TgUser


class Me(View):
    def get(self, request):
        username = request.user.username
        try:
            user = TgUser.objects.get(name=username)
            response = user.get_info()
        except TgUser.DoesNotExist:
            response = f'No registered telegram user with name {username}.'
        return HttpResponse(response)
