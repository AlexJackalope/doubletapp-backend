from telegram.ext import Updater
from telegram.ext import CommandHandler
from config.settings import TG_TOKEN
from .transport.bot.handlers import (start, set_phone, me)


def init_bot():
    updater = Updater(token=TG_TOKEN, use_context=True)
    start_handler = CommandHandler('start', start)
    phone_handler = CommandHandler('set_phone', set_phone)
    me_handler = CommandHandler('me', me)
    updater.dispatcher.add_handler(start_handler)
    updater.dispatcher.add_handler(phone_handler)
    updater.dispatcher.add_handler(me_handler)
    updater.start_polling()
