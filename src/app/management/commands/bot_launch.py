from django.core.management.base import BaseCommand
from ...internal.bot import init_bot


class Command(BaseCommand):
    def handle(self, *args, **options):
        init_bot()
